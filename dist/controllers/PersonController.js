'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _AbstractController = require('./AbstractController');

var _AbstractController2 = _interopRequireDefault(_AbstractController);

var _PersonRepository = require('../repositories/PersonRepository');

var _PersonRepository2 = _interopRequireDefault(_PersonRepository);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const repository = new _PersonRepository2.default();

class PersonController extends _AbstractController2.default {
    constructor() {
        super();
    }

    list(req, res) {
        super.list(req, res, repository);
    }

    create(req, res) {
        super.create(req, res, repository);
    }

    update(req, res) {
        super.update(req, res, repository);
    }

    delete(req, res) {
        super.delete(req, res, repository);
    }

    concatName(firstName, lastName) {
        return firstName + " " + lastName;
    }

}

exports.default = PersonController;
//# sourceMappingURL=PersonController.js.map