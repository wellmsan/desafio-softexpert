'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _express = require('express');

/* eslint-disable new-cap */
const routes = (0, _express.Router)();
/* eslint-enable new-cap */

/**
 * GET home page
 */
routes.get('/', (req, res) => {
  res.render('index', { title: 'Desafio SoftExpert | API' });
});

exports.default = routes;
//# sourceMappingURL=index.js.map