import Person from '../models/Person'
import AbstractRepository from './AbstractRepository'

class PersonRespository extends AbstractRepository{
    constructor(){
        super(Person)
    }

}

export default  PersonRespository