'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _Person = require('../models/Person');

var _Person2 = _interopRequireDefault(_Person);

var _AbstractRepository = require('./AbstractRepository');

var _AbstractRepository2 = _interopRequireDefault(_AbstractRepository);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

class PersonRespository extends _AbstractRepository2.default {
    constructor() {
        super(_Person2.default);
    }

}

exports.default = PersonRespository;
//# sourceMappingURL=PersonRepository.js.map