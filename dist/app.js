'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _morgan = require('morgan');

var _morgan2 = _interopRequireDefault(_morgan);

var _cookieParser = require('cookie-parser');

var _cookieParser2 = _interopRequireDefault(_cookieParser);

var _bodyParser = require('body-parser');

var _bodyParser2 = _interopRequireDefault(_bodyParser);

var _http = require('http');

var _http2 = _interopRequireDefault(_http);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _config = require('config');

var _config2 = _interopRequireDefault(_config);

var _SequelizeDB = require('../lib/SequelizeDB');

var _SequelizeDB2 = _interopRequireDefault(_SequelizeDB);

var _cors = require('cors');

var _cors2 = _interopRequireDefault(_cors);

var _ddTrace = require('dd-trace');

var _ddTrace2 = _interopRequireDefault(_ddTrace);

var _index = require('./routes/index');

var _index2 = _interopRequireDefault(_index);

var _PersonRoute = require('./routes/PersonRoute');

var _PersonRoute2 = _interopRequireDefault(_PersonRoute);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

require('dotenv').config();

const app = (0, _express2.default)();
app.use((0, _cors2.default)());

_ddTrace2.default.init();

app.use(_express2.default.static('public'));

const sequelizeDB = new _SequelizeDB2.default(process.env.DATABASE_CONNECTION_STRING);
sequelizeDB.connect();

app.disable('x-powered-by');
app.set('views', _path2.default.join(__dirname, '../views'));
app.set('view engine', 'ejs');
app.set('port', _config2.default.get('app.port'));

app.use((0, _morgan2.default)('dev'));
app.use(_bodyParser2.default.urlencoded({
    limit: '5mb',
    parameterLimit: 100000,
    extended: false
}));
app.use(_bodyParser2.default.json({
    limit: '5mb'
}));
app.use((0, _cookieParser2.default)());
app.use(_express2.default.static(_path2.default.join(__dirname, 'public')));

// Swagger Options
const expressSwagger = require('express-swagger-generator')(app);

let options = {
    swaggerDefinition: {
        info: {
            description: 'Desafio SoftExpert',
            title: 'Desafio SoftExpert | API',
            version: '1.0.0'
        },
        host: 'localhost:3000',
        produces: ["application/json", "application/xml"],
        schemes: ['http', 'https'],
        securityDefinitions: {
            JWT: {
                type: 'apiKey',
                in: 'header',
                name: 'x-access-token',
                description: ""
            }
        }
    },
    route: {
        url: process.env.BASE_PATH + '/docs',
        docs: process.env.BASE_PATH + '/api-docs.json'
    },
    basedir: __dirname, //app absolute path
    files: ['./routes/*.js', './models/*.js'] //Path to the API handle folder

};

expressSwagger(options);

app.use(process.env.BASE_PATH, _index2.default);
app.use(process.env.BASE_PATH + '/persons', _PersonRoute2.default);

// catch 404 and forward to error handler 
app.use((req, res, next) => {
    const err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
app.use((err, req, res, next) => {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

const server = _http2.default.createServer(app);
const port = app.get('port');

server.listen(port, () => {
    console.log(`Application listening on ${_config2.default.get('app.baseUrl')}`);
    console.log(`Environment => ${_config2.default.util.getEnv('NODE_ENV')}`);
});

exports.default = app;
//# sourceMappingURL=app.js.map