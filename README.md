# Desafio SoftExpert

Esse projeto foi desenvolvido em NodeJS/ExpressJS, para implantação fácil em clusters EKS da Amazon:

-  ExpressJS, framework para construção de apis;
-  SequelizeDB, ORM;
-  Banco de Dados PostgreSQL;
-  Swagger, para documentação da API
-  Gitlab, repositório de código;
-  Gitlab, armazenamento da imagem docker;
-  Travis-CI no processo de CI/CD

## Pré-Requisitos
-  Instalar e configurar o AWS-CLI (https://docs.aws.amazon.com/pt_br/streams/latest/dev/kinesis-tutorial-cli-installation.html)
-  Gerar o Person Token, do Gitlab, com todas as permissões (https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html)


## Como reproduzir:

1.  Faça um Fork do projeto;
2.  Crie uma conta em https://travis-ci.com/;
3.  Importe seu repositorio no Travis-IC;
4.  Crie as variáveis de ambiente, no Travis-CI:

-  AWS_ACCESS_KEY_ID (ID gerado pelo aws config)
-  AWS_SECRET_ACCESS_KEY (KEY gerado pelo aws config)
-  AWS_DEFAULT_REGION (Zona/Região da sua conta AWS)
-  CI_REGISTRY_TOKEN (PersonToken Gitlab)
-  CI_REGISTRY_USER  (Usuário do Gitlab)
-  GITLAB_USER_EMAIL (Email utilizado na conta do Gitlab)
-  CLUSTER_NAME (Nome do cluster eks)

5. No arqivo, service.yaml, Altere as variáveis de ambiente que serão utilizadas pelo container:
```
...
env:
- name: APP_NAME
value: "<APP_NAME>"
- name: BASE_PATH
value: "desafio-softexpert"
- name: DB_HOST
value: "<DB_HOST>"
- name: DB_NAME
value: "<DB_NAME>"
- name: DB_USER
value: "<DB_USER>"
- name: DB_PASS
value: "<DB_PASS>"
- name: DB_PORT
value: "<DB_PORT>"
....
```
6. Faça commit no repositorio ou force o Build o projeto no Travis-CI.

O acesso ao serviço deve ser realizado em: <HOST_URL>/desafio-softexpert

### Endpoints
```
/*
* Listar todas as Pessoas
*/
GET /persons 
```
```
/*
* Buscar pesssoa por por id
*/
GET /persons/:id
```
```
/*
* Buscar pessoa por query
*/
GET /persons/?first_name=joao
GET /persons/?last_name=macedo
GET /persons/?age=15
```
```
/*
* Salvar Pessoa
*/
POST /persons/
{
    first_name: 'João'
    first_name: 'Macedo'
    age: 19
}
```
```
/*
* Atualizar Pessoa
*/
PUT /persons/:id
{
    first_name: 'João'
    first_name: 'Macedo'
    age: 19
}
```
```
/*
* Excluir Pessoa
*/
DELETE /persons/:id
```
O Swagger por ser acessado no diretório <HOST_URL>/desafio-softexpert/docs

7. Be Happy!