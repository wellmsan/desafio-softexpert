import AbstractController from './AbstractController'
import PersonRepository from '../repositories/PersonRepository'

const repository = new PersonRepository()

class PersonController extends AbstractController {
    constructor() {
        super()
    }

    list(req, res){
        super.list(req, res, repository)
    }

    create(req, res) {
        super.create(req, res, repository)
    }

    update(req, res) {
        super.update(req, res, repository)
    }

    delete(req, res) {
        super.delete(req, res, repository)
    }

    concatName(firstName, lastName){
        return firstName + " " + lastName
    }

}

export default PersonController