"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _sequelize = require("sequelize");

/**
 * @typedef Person
 * @property { integer } id.required
 * @property { string } first_name.required
 * @property { string } last_name.required
 * @property { integer } age.required
 */
class Person extends _sequelize.Sequelize.Model {
    static init(sequelize, DataTypes) {
        return super.init({
            first_name: {
                type: DataTypes.STRING(20)
            },
            last_name: {
                type: DataTypes.STRING(20)
            },
            age: {
                type: DataTypes.INTEGER
            }
        }, {
            tableName: "persons",
            modelName: "person",
            underscored: true,
            sequelize
        });
    }
    static associate(models) {}
}

exports.default = Person;
//# sourceMappingURL=Person.js.map