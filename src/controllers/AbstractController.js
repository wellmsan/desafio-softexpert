class AbstractController {

    constructor() {
        if (new.target === AbstractController) {
            throw new TypeError("Cannot construct Abstract instances directly")
        }
    }

    list(req, res, repository) {
        if (!req.params.id) {
            repository.list(req.query)
                .then(async (data) => {
                    res.status(200).json(data);
                }, (e) => {
                    res.status(500).send({ message: 'Error List: ' + e.message });
                });
        } else {
            repository.get(req.params.id)
                .then(async (data) => {
                    res.status(200).json(data);
                }, (e) => {
                    res.status(500).send({ message: 'Error Get: ' + e.message });
                });
        }
    }

    create(req, res, repository) {
        try {
            repository.create(req.body).then(data => {
                res.status(201).send({ message: 'Registro cadastrado!', data: data })
            })

        } catch (e) {
            res.status(500).send({ message: 'Error Create: ' + e.message })
        }
    }

    update(req, res, repository) {
        try {
            repository.update(req.query.id, req.body)
            res.status(201).send({ message: 'Registro atualizado!' })
        } catch (e) {
            res.status(500).send({ message: 'Error Update: ' + e.message })
        }
    }

    delete(req, res, repository) {
        try {
            repository.delete(req.query.id)
            res.status(201).send({ message: 'Registro excuído!' });
        } catch (e) {
            res.status(500).send({ message: 'Error Delete: ' + e.message })
        }
    }

}

export default AbstractController